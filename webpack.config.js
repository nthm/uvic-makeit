const webpack = require('webpack')
const path = require('path')

const config = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    // relative path for files built by webpack
    publicPath: '/build/',
    // absolute path for all other files
    contentBase: __dirname,
    hot: true,
  },
  // don't polyfill anything from Node
  node: {
    process: false,
    console: false,
    global: false,
    __filename: false,
    __dirname: false,
    Buffer: false,
    setImmediate: false,
  },
}

module.exports = config
