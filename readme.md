# GoMake

A small match maker application for students and teams. My roommate wanted his
project idea off the ground, and I wanted an excuse to learn Mithril. Written in
an evening - not proud of all of it yet.

Create, delete, and list both users and projects (teams). Display a list of all
skills both being sought by projects and provided by users.

![](./screenshot.png)

## Running

Use the usual `npm run build` and `npm run start` to launch the application. The
start command will do hot reloading as expected.

## Backend

This application is written with Mithril, and currently has no backend. It
connects to [REM](https://rem-rest-api.herokuapp.com/) for fake data, which
means created projects and users are stored in a cookie for the duration of the
session. For now. A Python backend is being developed elsewhere.
