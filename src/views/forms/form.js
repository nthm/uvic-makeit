import m from 'mithril'

const Form = {
  view(vnode) {
    const { model, intro, fields } = vnode.attrs
    return (
      m('form', {
        onsubmit: e => {
          e.preventDefault()
          model.save()
        },
      }, [
        m('a.btn[href=/]', {
          oncreate: m.route.link,
        }, 'Home'),
        intro,
        m('p', 'Please fill in all the fields below'),
        m('hr'),
        fields.map(field =>
          m('label', [
            field,
            m(`input[placeholder=${field}]`, {
              oninput: m.withAttr('value', value => {
                model.current[field] = value
              }),
              value: model.current[field],
            }),
          ])),
        m('button.button[type=submit]', 'Save'),
      ])
    )
  },
}

export default Form
