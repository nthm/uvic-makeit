import m from 'mithril'
import Projects from '../../models/projects'
import Form from './form'

const fields = [
  'Name',
  'Description',
  'Current member count',
  'Age',
  'Website',
  'Looking for...',
]
const ProjectForm = {
  // load the requested project
  oninit: vnode => Projects.load(vnode.attrs.id),
  view() {
    if (!Projects.current) {
      setTimeout(() => {
        Projects.current = {}
      }, 400)
      return 'Loading'
    }
    return (
      m(Form, {
        model: Projects,
        intro: m('h2', [
          Projects.current.name
            ? `Editing ${Projects.current.name}`
            : 'New project',
        ]),
        fields,
      })
    )
  },
}

export default ProjectForm
