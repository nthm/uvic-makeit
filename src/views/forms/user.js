import m from 'mithril'
import Users from '../../models/users'
import Form from './form'

const fields = [
  'Name',
  'Email',
  'NetLink ID',
  'Major',
  'Year of study',
  'Website',
  'Bio',
  'Skills',
]
const UserForm = {
  // load the requested user
  oninit: vnode => Users.load(vnode.attrs.id),
  view() {
    return (
      m(Form, {
        model: Users,
        intro: m('h2', [
          Users.current.name
            ? `Editing ${Users.current.name}`
            : 'New user',
        ]),
        fields,
      })
    )
  },
}

export default UserForm
