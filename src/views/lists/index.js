import m from 'mithril'
import FoobarIpsum from 'foobar-ipsum'

import Users from '../../models/users'
import Projects from '../../models/projects'

const generator = new FoobarIpsum({
  size: {
    sentence: 20,
    paragraph: 4,
  },
})

const ListPage = {
  view(vnode) {
    return [
      m('nav.container', [
        m('a', {
          href: '/',
          oncreate: m.route.link,
        }, 'Home'),
        m('a', {
          href: '/users',
          oncreate: m.route.link,
        }, 'Users'),
        m('a', {
          href: '/projects',
          oncreate: m.route.link,
        }, 'Projects'),
      ]),
      m('.container[style=padding:20px 0]', vnode.children),
    ]
  },
}

const UserList = {
  oninit: Users.loadList,
  view() {
    return (
      m(ListPage, [
        m('.list', Users.list.map(user =>
          m('a.list-item', {
            href: `/user/${user.id}`,
            oncreate: m.route.link,
          }, [
            m('.pic', user.name.charAt(0).toUpperCase()),
            m('.text', [
              m('h3', user.name),
              m('p', generator.sentence()),
            ]),
          ]))),
      ])
    )
  },
}

const ProjectList = {
  oninit: Projects.loadList,
  view() {
    return (
      m(ListPage, [
        m('.list', Projects.list.length > 0
          ? Projects.list.map((project, index) =>
            m('a.list-item', {
              href: `/project/${index}`,
              oncreate: m.route.link,
            }, [
              m('[style=float:right;margin:10px]', [
                m(`a.btn[href=${project.Website}]`, 'Website'),
                m(`a.btn[href=${project.Github}]`, 'Github'),
              ]),
              m('h3', project.Name),
              m('p', project.Founder),
              m('p', project.Type),
              m('p', project.Bio),
            ]))
          : m('div', {
            style: 'text-align:center;margin-bottom:20px;',
          }, [
            'No projects',
            m('a', {
              href: '/project/0',
              oncreate: m.route.link,
            }, [
              m('button.button[type=button]', 'New project'),
            ]),
          ])),
      ])
    )
  },
}

export { UserList, ProjectList }
