import m from 'mithril'

const Users = {
  list: [],
  current: {},
  loadList() {
    return m.request({
      method: 'GET',
      url: 'https://rem-rest-api.herokuapp.com/api/users',
      withCredentials: true,
    })
      .then(result => {
        Users.list = result.data.map(obj => {
          obj.name = `${obj.firstName} ${obj.lastName}`
          return obj
        })
      })
  },
  load(id) {
    return m.request({
      method: 'GET',
      url: `https://rem-rest-api.herokuapp.com/api/users/${id}`,
      withCredentials: true,
    })
      .then(result => {
        result.name = `${result.firstName} ${result.lastName}`
        Users.current = result
      })
  },
  save() {
    const { id } = Users.current
    return m.request({
      method: 'PUT',
      url: `https://rem-rest-api.herokuapp.com/api/users/${id}`,
      data: Users.current,
      withCredentials: true,
    })
  },
}

export default Users
