import m from 'mithril'

const Projects = {
  list: [],
  current: {},
  loadList() {
    return m.request({
      method: 'GET',
      url: '/res/data.json',
    })
      .then(data => {
        Projects.list = data
      })
  },
  load(id) {
    Projects.current = Projects.list[id] || {}
  },
  save() {
    // not implemented
  },
}

export default Projects
