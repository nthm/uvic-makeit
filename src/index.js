import m from 'mithril'

import Projects from './models/projects'

import { UserForm, ProjectForm } from './views/forms'
import { UserList, ProjectList } from './views/lists'

const Button = {
  view(vnode) {
    const { children } = vnode
    return (
      m('a', {
        href: vnode.attrs.to,
        oncreate: m.route.link,
      }, [
        m('.btn.senders', children),
      ])
    )
  },
}

m.route.prefix('#')
m.route(document.body, '/', {
  // the list is show on the main page
  '/': {
    // trying out different styles of function calls, sorry
    render: () =>
      m('main', [
        m('header', [
          m('.container', [
            m('h1', 'GoMake'),
            m('p', 'Gomake is a platform for early stage startup founders, clubs, and hackathon teams to connect with talent on their campus; and for students who want more out of the university experience to get involved in a project that excites them.'),
          ]),
        ]),
        m('section.container', [
          m(Button, { to: '/users' }, 'Find People'),
          m(Button, { to: '/projects' }, 'Find Projects'),
          m(Button, { to: `/project/${Projects.list.length}` }, 'Post Project'),
        ]),
      ]),
  },
  '/users': {
    render: () => m(UserList),
  },
  '/projects': {
    render: () => m(ProjectList),
  },
  '/user/:id': {
    render: vnode => m('.card.container', m(UserForm, vnode.attrs)),
  },
  '/project/:id': {
    render: vnode => m('.card.container', m(ProjectForm, vnode.attrs)),
  },
})
